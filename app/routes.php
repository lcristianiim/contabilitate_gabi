<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('homepage');
});

Route::get('homepage', function()
{
    return View::make('homepage');
});

Route::get('despre_mine', function(){
    return View::make('despre_mine');
});

Route::get('linkuri_utile', function(){
    return View::make('linkuri_utile');
});

Route::get('contact', function(){
    return View::make('contact');
});

Route::get('servicii_de_contabilitate', function(){
    return View::make('contabilitate');
});

Route::get('taxe_si_impozite', function(){
    return View::make('taxe_si_impozite');
});

Route::get('resurse_umane', function(){
    return View::make('resurse_umane');
});
