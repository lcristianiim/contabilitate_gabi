@extends('layouts.general_pages.main_user_page')

@section('content_1')
<div class="row">
    <div class="col-sm-4">
        <div class="jumbotron">
            <img class="width_80_procent" src="<?=asset('img/logo_parafa.png')?>" alt=""/>
            <img class="width_100_procent" src="<?=asset('img/logo_text.png')?>" alt=""/>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="row">
            <div class="col-sm-12">
                <h2><span class="text-muted">Taxe si impozite la stat</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis.  </p>
            </div>
        </div>

        <div class="row-fluid">
            <h4>Primul element </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis. </p>
        </div>

        <div class="row-fluid">
            <h4>Al doilea element</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis. </p>
        </div>

        <div class="row-fluid">
            <h4>Al treilea element</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis. </p>
        </div>

        <div class="row-fluid">
            <h4>Al patrulea element</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis. </p>
        </div>
    </div>
</div>
@endsection
