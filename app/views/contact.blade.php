@extends('layouts.general_pages.main_user_page')

@section('content_1')
<div class="row">
    <div class="col-sm-4">
        <div class="jumbotron">
            <img class="width_80_procent" src="<?=asset('img/logo_parafa.png')?>" alt=""/>
            <img class="width_100_procent" src="<?=asset('img/logo_text.png')?>" alt=""/>
        </div>
    </div>
    <div class="col-sm-8">

        <div class="row">
            <h2 class="text-center"><span class="text-muted">Contact</span></h2>
        </div>
        <div class="row-fluid">
            <h3>Telefon: </h3>
            <p>0724 051 201</p>
        </div>

        <div class="row-fluid">
            <h3>Fax: </h3>
            <p>0724 051 201</p>
        </div>

        <div class="row-fluid">
            <h3>Email: </h3>
            <p>lcristianiim@yahoo.com</p>
        </div>

        <div class="row-fluid">
            <a href="www.mfinante.ro">www.contabilitategabi.ro</a>
            <p>Contactul telefonic pana la ora 17:00 </p>
        </div>


    </div>
</div>
@endsection
