@extends('layouts.master_page')

@section('head')
<link rel="stylesheet" href="<?=asset('css/jumbotron-narrow.css')?>"/>
    @yield('head_1')
@endsection

@section('content')
<div class="row">
    <div class="header">
        <ul class="nav nav-pills red pull-right">
            <li class="{{ Request::is('/') ? 'active' : '' }} {{ Request::is('homepage') ? 'active' : '' }}"><a href="homepage">Acasa</a></li>
            <li class="{{ Request::is('despre_mine') ? 'active' : '' }}" ><a href="despre_mine">Despre mine</a></li>
            <li class="{{ Request::is('linkuri_utile') ? 'active' : '' }}"><a href="linkuri_utile">Linkuri utile</a></li>
            <li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="contact">Contact</a></li>
        </ul>
        <h3 class="text-muted">Servicii profesionale de contabilitate</h3>
    </div>
</div>


    @yield('content_1')
@endsection

@section('footer')
<div class="container">
    <div class="row footer">
        <div class="col-sm-4">
            <p>&copy; 2014 Gabriela Szabo</p>
        </div>
    </div>
    @yield('footer_1')
</div>

@endsection






