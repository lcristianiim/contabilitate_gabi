<!DOCTYPE HTML>
<html>

<head>
	<!--
****************************************************
(c) WebSite Name

(c)2014 Cristian
        lcristianiim@yahoo.com

Design & Development by Cristian

****************************************************
-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta charset="utf-8">

	<meta name="author" content="">
	<meta name="publisher" content="">
	<meta name="page-topic" content="">
	<meta name="page-type" content="">
	<meta name="keywords" lang="en" content="">
	<meta name="description" lang="en" content="">

<!--  CSS  -->
    <link rel="stylesheet" href="<?=asset('bootstrap-3.1.0/bootstrap-3.1.0/dist/css/bootstrap.min.css')?>"/>
    <link rel="stylesheet" href="<?=asset('font-awesome-4.2.0/css/font-awesome.min.css')?>"/>
    <link rel="stylesheet" href="<?=asset('css/custom.css')?>"/>

<!-- SCRIPTS -->
    <script src="<?=asset('js/jquery-1.9.min.js')?>"></script>

    @yield('head')

</head>

<body>
    <div class="container">
        @yield('content')

        <div class="row">
            <footer class="margin_top_50">
                @yield('footer')
            </footer>
        </div>

    </div>



</body>


</html>