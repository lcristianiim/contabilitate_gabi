@extends('layouts.general_pages.main_user_page')

@section('content_1')
<div class="row">
    <div class="col-sm-4">
        <div class="jumbotron">
            <img class="width_80_procent" src="<?=asset('img/logo_parafa.png')?>" alt=""/>
            <img class="width_100_procent" src="<?=asset('img/logo_text.png')?>" alt=""/>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="row">
            <div class="col-sm-4 ">
                <a href="#" class="thumbnail width_150_150 custom_center">
                    <img class="width_150_150" src="<?=asset('img/PozaCV.jpg')?>" alt="...">
                </a>
            </div>
            <div class="col-sm-8">
                <h2><span class="text-muted">Szabo Cristian Marcel</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis.  </p>
            </div>
        </div>

        <div class="row-fluid">
            <h3>Primul detaliu despre mine</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis. </p>
        </div>

        <div class="row-fluid">
            <h3>A doua chestie despre mine</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis. </p>
        </div>

        <div class="row-fluid">
            <h3>A treia chestie despre mine</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis. </p>
        </div>

        <div class="row-fluid">
            <h3>A patra chestie despre mine</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula tempus imperdiet. Aenean ornare felis quam. Aenean turpis arcu, posuere suscipit porta vel, mattis vitae elit. Curabitur mollis, justo non venenatis. </p>
        </div>


    </div>
</div>
@endsection
