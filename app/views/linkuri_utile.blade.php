@extends('layouts.general_pages.main_user_page')

@section('content_1')
<div class="row">
    <div class="col-sm-4">
        <div class="jumbotron">
            <img class="width_80_procent" src="<?=asset('img/logo_parafa.png')?>" alt=""/>
            <img class="width_100_procent" src="<?=asset('img/logo_text.png')?>" alt=""/>
        </div>
    </div>
    <div class="col-sm-8">

        <div class="row">
            <h2 class="text-center"><span class="text-muted">Linkuri utile</span></h2>
        </div>
        <div class="row-fluid">
            <a href="www.mfinante.ro">www.mfinante.ro</a>
            <p>Site-ul oficial al ministerului de finante Romania. </p>
        </div>

        <div class="row-fluid">
            <a href="www.mfinante.ro">www.altsite.ro</a>
            <p>Site-ul oficial al altui minister din Romania. </p>
        </div>


    </div>
</div>
@endsection
