@extends('layouts.general_pages.main_user_page')

@section('head_1')
    <!--Here goes specific css and scripts-->
    <title>Homepage</title>

@endsection

@section('content_1')
<div class="jumbotron">
    <div class="row">
        <div class="col-sm-3">
            <img class="width_80_procent"  src="<?=asset('img/logo_parafa.png')?>" alt="homepage"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <img class="width_70_procent"  src="<?=asset('img/logo_text.png')?>" alt="homepage"/>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <h4>Contabilitate</h4>
                </div>
                <div class="col-sm-4">
                    <h4>Calcul taxe si impozite</h4>
                </div>
                <div class="col-sm-4">
                    <h4>Resurse umane</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <h2>Contabilitate</h2>
        <p>Foarte tari sunt serviciile de contabilitate</p>
        <p><a role="button" href="servicii_de_contabilitate" class="btn btn-default">Detalii »</a></p>
    </div>
    <div class="col-sm-4">
        <h2>Calcul taxe si impozite</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a role="button" href="taxe_si_impozite" class="btn btn-default">Detalii »</a></p>
    </div>
    <div class="col-sm-4">
        <h2>Resurse umane</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a role="button" href="resurse_umane" class="btn btn-default">Detalii »</a></p>
    </div>
</div>

@endsection

@section('footer_1')
<!--specific elements of the footer-->

@endsection